import { Component } from '@angular/core';
import { tripSorterService } from './core/travel/services/travel.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tripSorter';
  constructor(private tripSorterService: tripSorterService) {
    tripSorterService.loadCities();
  }
}
