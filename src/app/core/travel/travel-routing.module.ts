import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchTravelComponent } from './components/search-travel/search-travel.component';

const routes: Routes = [
  {
    path: '',
    component: SearchTravelComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TravelRoutingModule {}
