import { TestBed } from '@angular/core/testing';

import { tripSorterService } from './tripSorter.service';

describe('tripSorterService', () => {
  let service: tripSorterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(tripSorterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
