import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

export enum ESuggestionOptions {
  cheapest = 'cheapest',
  fastest = 'fastest',
}

@Injectable({
  providedIn: 'root',
})
export class tripSorterService {
  availableCities = [];
  constructor(private http: HttpClient) {}

  loadCities() {
    return this.http
      .get(`${environment.apiUrl}/travel/cities`)
      .pipe(
        catchError((err) => {
          console.log('[Error] with fetching cities: ', err);
          return EMPTY;
        })
      )
      .subscribe((cities: string[]) => {
        this.availableCities = cities;
      });
  }

  getSuggestion(
    from: string,
    to: string,
    suggestedBy = ESuggestionOptions.cheapest
  ) {
    return this.http.get(
      `${environment.apiUrl}/travel?from=${from}&to=${to}&suggestedBy=${suggestedBy}`
    );
  }
}
