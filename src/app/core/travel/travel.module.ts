import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { SuggestResultComponent } from './components/suggest-results/suggest-result.component';
import { SearchTravelComponent } from './components/search-travel/search-travel.component';
import { TravelRoutingModule } from './travel-routing.module';

@NgModule({
  declarations: [SearchTravelComponent, SuggestResultComponent],
  imports: [CommonModule, TravelRoutingModule, SharedModule],
})
export class TravelModule {}
