import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchtripSorterComponent } from './search-tripSorter.component';

describe('SearchtripSorterComponent', () => {
  let component: SearchtripSorterComponent;
  let fixture: ComponentFixture<SearchtripSorterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchtripSorterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchtripSorterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
