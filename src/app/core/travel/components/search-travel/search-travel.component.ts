import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, Subscription } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { fadeAnimation } from 'src/app/shared/animations';
import { capitalize } from '../../../../shared/utils';
import { ISuggesstion } from '../../dto';
import {
  ESuggestionOptions,
  tripSorterService,
} from '../../services/travel.service';

@Component({
  selector: 'app-search-travel',
  templateUrl: './search-travel.component.html',
  styleUrls: ['./search-travel.component.scss'],
  animations: [fadeAnimation],
})
export class SearchTravelComponent implements OnInit, OnDestroy {
  errorMessage;
  loading = false;
  availableCities: string[] = [];
  suggestOptions = ESuggestionOptions;
  searchForm: FormGroup;
  bestSuggestion: ISuggesstion;
  formChanged = true;
  subs: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private tripSorterService: tripSorterService
  ) {}

  get suggestedBy() {
    return this.searchForm.get('suggestBy').value;
  }

  ngOnInit(): void {
    // Listen for from changes

    // Load all cities
    this.availableCities = this.tripSorterService.availableCities;
    // Extract search options from params
    const {
      from = '',
      to = '',
      suggestedBy = ESuggestionOptions.cheapest,
    } = this.route.snapshot.queryParams;

    this.searchForm = this.fb.group({
      from: this.fb.control(capitalize(from), Validators.required),
      to: this.fb.control(capitalize(to), Validators.required),
      suggestBy: this.fb.control(
        suggestedBy.toLowerCase(),
        Validators.required
      ),
    });

    this.subs.push(
      this.searchForm.valueChanges.subscribe((res) => (this.formChanged = true))
    );
  }

  updateSuggestionType(type: ESuggestionOptions) {
    this.searchForm.patchValue({
      suggestBy: type,
    });
  }

  getSuggestion() {
    const { from, to, suggestBy } = this.searchForm.value;

    this.errorMessage = '';
    this.loading = true;

    if (from == '' || to == '') {
      this.errorMessage = 'Invalid selections!';
      return;
    }

    if (this.formChanged) {
      console.log('Send request: ');

      this.formChanged = false;
      this.tripSorterService
        .getSuggestion(from, to, suggestBy)
        .pipe(
          catchError((err) => {
            this.errorMessage = err.error;
            this.loading = false;
            return EMPTY;
          })
        )
        .subscribe((res: ISuggesstion) => {
          this.bestSuggestion = {...res};
          this.loading = false;
        });
    }
  }

  onSelectionChange() {
    const formValue = this.searchForm.value;
    if (formValue.from == '' && formValue.to == '') {
      this.errorMessage = 'Invalid selections!';
      return;
    } else {
      this.errorMessage = '';
    }
  }

  reverse() {
    this.searchForm.patchValue({
      from: this.searchForm.value.to,
      to: this.searchForm.value.from,
    });
  }

  reset() {
    this.searchForm.patchValue({
      from: '',
      to: '',
    });
    this.bestSuggestion = undefined;
  }

  ngOnDestroy(): void {
    // Clear all subscriptions on destory
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
