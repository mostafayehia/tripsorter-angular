import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { fadeAnimation, listAnimation } from 'src/app/shared/animations';
import { ISuggesstion } from '../../dto';
import { ESuggestionOptions } from '../../services/travel.service';

@Component({
  selector: 'suggest-result',
  templateUrl: './suggest-result.component.html',
  styleUrls: ['./suggest-result.component.scss'],
  animations: [listAnimation, fadeAnimation],
})
export class SuggestResultComponent implements OnInit, OnChanges {
  optemizeMessage;
  suggestions = [];
  @Input() suggestedBy = ESuggestionOptions.cheapest;
  @Input() suggestion: ISuggesstion = {
    cost: 0,
    duration: 0,
    route: [],
  };
  @Output() onReset: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes?.suggestion) {
      this.updateOptemizeMessage();
      this.suggestions = [];
      setTimeout(() => {
        this.suggestions = this.suggestions.length
          ? []
          : [...changes.suggestion.currentValue.route];
      }, 0);
    }
  }

  ngOnInit() {}

  private updateOptemizeMessage() {
    this.optemizeMessage =
      this.suggestedBy == ESuggestionOptions.cheapest
        ? 'This is the cheapest trip, optemized by fastest'
        : 'This is the fastest trip, optemized by cheapest';
  }

  reset() {
    this.onReset.emit(1);
  }
}
