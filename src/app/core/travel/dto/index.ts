export interface Duration {
  h: string;
  m: string;
}

export interface IDeal {
  transport: string;
  departure: string;
  arrival: string;
  duration: Duration;
  cost: number;
  discount: number;
  reference: string;
}

export interface ISuggesstion {
  route: IDeal[];
  cost: number;
  duration: number;
}
