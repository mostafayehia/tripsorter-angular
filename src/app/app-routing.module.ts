import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'travel',
    pathMatch: 'full',
  },
  {
    path: 'travel',
    loadChildren: () =>
      import('./core/travel/travel.module').then((m) => m.TravelModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
